// 五、请说出下列最终输出的结果，并解释为什么？

var a = 10;
var obj = {
  a: 20,
  fn() {
    console.log(this.a)
    setTimeout(() => {
      console.log(this.a);
    });
  }
};
obj.fn();

// 答：结果为 20
// 原因：
// setTimeout 中定义的方法使用箭头函数定义会改变 this 的指向，默认指向定义它时，所处上下文的对象的 this 指向，此时 this 指向 调用它的 fn 方法
// 而 fn 方法的定义为 function () {} 的缩写，为普通函数，fn 中的 this 指向调用它的 obj, 所以 this.a === obj.a === 20
