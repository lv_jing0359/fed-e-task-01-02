// 一、请说出下列最终的执行结果，并解释为什么
var a = [];
for (var i = 0; i < 10; i++) {
  a[i] = function () {
    console.log(i);
  };
}

a[6]();

// 答：结果为 10
// 原因：因为这里通过 var 定义的变量 i 为一个全局变量，function 方法调用时，for 循环已经执行完成，i 已经变成了 10，所以结果为 10