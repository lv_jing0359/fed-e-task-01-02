// 三、结合 ES6 新语法，用最简单的方式找出数组中的最小值？

var arr = [12, 24, 32, 89, 4]

const minValue = Math.min(...arr)

console.log(minValue) // 4
